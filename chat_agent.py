import llama_index.core
from llama_index.core.tools import QueryEngineTool, ToolMetadata
import chainlit as cl
from chainlit.input_widget import Select, TextInput
import anthropic, os
from llama_index.core.agent import ReActAgent
from llama_index.llms.anthropic import Anthropic
from llama_index.core.callbacks import CallbackManager
from index_wikipages import create_index
from utils import get_apikey


index = None
c = anthropic.AsyncAnthropic(api_key=os.environ["ANTHROPIC_API_KEY"])

@cl.on_chat_start
async def on_chat_start():
    global index
    # Settings
    settings = await cl.ChatSettings(
        [
            Select(
                id="MODEL",
                label="Claude3 Model",
                values=["claude-3-opus-20240229", "claude-3-sonnet-20240229", "claude-3-haiku-20240307"],
                initial_index=0,
            ),
            TextInput(id="WikiPageRequest", label="Request Wikipage")
        ]
    ).send()
    await cl.Avatar(
        name="Claude",
        url="https://www.anthropic.com/images/icons/apple-touch-icon.png"
    ).send()
    cl.user_session.set(
        "prompt_history",
        "",
    )


def wikisearch_engine(index):
    query_engine = index.as_query_engine(
        response_mode="compact", verbose=True, similarity_top_k=10
    )
    return query_engine


def create_react_agent(MODEL):
    query_engine_tools = [
        QueryEngineTool(
            query_engine=wikisearch_engine(index),
            metadata=ToolMetadata(name="Wikipedia", 
            description="Useful for performing searches on the Wikipedia knowledgebase."
            ),
        )
    ]

    llm = Anthropic(model=MODEL)
    agent = ReActAgent.from_tools(
        tools=query_engine_tools,
        llm=llm,
        callback_manager=CallbackManager([cl.LlamaIndexCallbackHandler()]),
        verbose=True,
    )
    return agent


@cl.on_settings_update
async def setup_agent(settings):
    global agent
    global index
    query = settings["WikiPageRequest"]
    index = create_index(query)

    print("on_settings_update", settings)
    MODEL = settings["MODEL"]
    agent = create_react_agent(MODEL)
    await cl.Message(
        author="Claude", content=f"""Wikipage(s) "{query}" successfully indexed"""
    ).send()


@cl.on_message
async def chat(message: cl.Message):
    if agent:
        response = await cl.make_async(agent.chat)(message.content)
        await cl.Message(author="Claude", content=response).send()