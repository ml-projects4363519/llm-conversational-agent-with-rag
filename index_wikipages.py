import anthropic, os
from llama_index.llms.anthropic import Anthropic
from llama_index.core import VectorStoreIndex, ServiceContext, Settings
from llama_index.core.node_parser import SentenceSplitter
from llama_index.core.output_parsers import PydanticOutputParser
from llama_index.readers.wikipedia import WikipediaReader
from llama_index.core.program import LLMTextCompletionProgram
from pydantic import BaseModel


os.environ["ANTHROPIC_API_KEY"] = "sk-ant.."


tokenizer = Anthropic().tokenizer
llm = Anthropic(model="claude-3-opus-20240229", temperature=0.1)
Settings.llm = llm
Settings.embed_model = "local:BAAI/bge-small-en-v1.5"
Settings.tokenizer = tokenizer


# define the data model in pydantic
class WikiPageList(BaseModel):
    "Data model for WikiPageList"
    pages: list


def wikipage_list(query):
    prompt_template_str = """
    Given the input {query}, 
    extract the Wikipedia pages mentioned after 
    "please index:" and return them as a list.
    If only one page is mentioned, return a single element list.
    """
    program = LLMTextCompletionProgram.from_defaults(
        output_cls=WikiPageList,
        prompt_template_str=prompt_template_str,
        verbose=True,
    )

    wikipage_requests = program(query=query)

    return wikipage_requests


def create_wikidocs(wikipage_requests):
    loader = WikipediaReader()
    documents = loader.load_data(wikipage_requests.pages)
    #print("\ndocs:", documents)    
    return documents


def create_index(query):
    global index
    wikipage_requests = wikipage_list(query)
    documents = create_wikidocs(wikipage_requests)
    index = VectorStoreIndex.from_documents(
        documents,
        transformations=[SentenceSplitter(chunk_size=1024, chunk_overlap=20)],
        show_progress=True, 
        )
    return index


if __name__ == "__main__":
    query = "/get wikipages: paris, lagos, lao"
    index = create_index(query)
    print("INDEX CREATED", index)